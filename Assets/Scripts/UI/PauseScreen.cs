﻿/*
* 
* Carlos Adan Cortes De la Fuente
* All rights reserved. Copyright (c) 
* Email: krlozadan@gmail.com
*
*/

using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseScreen : UIScreen 
{

	#region Screen Life Cycle

		protected override void UIScreenEnabled() 
		{
			GameManager.Instance.PauseGame();
		}

		protected override void UIScreenDisabled()
		{
			GameManager.Instance.ResumeGame();
		}

	#endregion

	#region Events


		public void OnResumeGameButtonPressed()
		{
			UIManager.Instance.ShowScreen<GameScreen>();
		}

		public void OnQuitGameButtonPressed()
		{
			SceneManager.LoadScene("MainMenu");
			AudioManager.Instance.StopAll();
			UIManager.Instance.ShowScreen<MainMenuScreen>();
		}

    #endregion
}