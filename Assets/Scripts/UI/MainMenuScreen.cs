﻿/*
* 
* Carlos Adan Cortes De la Fuente
* All rights reserved. Copyright (c) 
* Email: krlozadan@gmail.com
*
*/

using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuScreen : UIScreen 
{	
	
	#region Screen Life Cycle

		protected override void UIScreenEnabled()
		{
			StartCoroutine(StatePlayIntro());
		}

		protected override void UIScreenDisabled()
		{
			// Do something on disable
		}

	#endregion

	#region Events
	
		public void OnStartGameMenuPressed()
		{
			SceneManager.LoadScene("Game");
			AudioManager.Instance.PlayEvent("Play_Audience_Loop_1");
			UIManager.Instance.ShowScreen<GameScreen>();
		}

		public void OnCreditsMenuPressed()
		{	
			UIManager.Instance.ShowScreen<CreditsScreen>();
		}

		public void OnInstructionsMenuPressed()
		{
			UIManager.Instance.ShowScreen<InstructionsScreen>();
		}

		public void OnQiutMenuPressed()
		{
			Application.Quit();
		}
	
	#endregion

	IEnumerator StatePlayIntro()
	{
		while (AudioManager.Instance == null)
		{
			yield return null;
		}
		AudioManager.Instance.PlayEvent("Play_Music_Loop_Menu");
		yield break;
	}

}