﻿/*
* 
* Carlos Adan Cortes De la Fuente
* All rights reserved. Copyright (c) 
* Email: krlozadan@gmail.com
*
*/

using UnityEngine;
using UnityEngine.SceneManagement;

public class InstructionsScreen : UIScreen 
{	
	
	#region Screen Life Cycle

		protected override void UIScreenEnabled()
		{

		}

		protected override void UIScreenDisabled()
		{
			
		}

	#endregion

	#region Events

		public void OnBackButtonMenuPressed()
		{
			UIManager.Instance.ShowScreen<MainMenuScreen>();
		}
	
	#endregion

}