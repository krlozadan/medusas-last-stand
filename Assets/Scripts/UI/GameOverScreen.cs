﻿
/*
* 
* Carlos Adan Cortes De la Fuente
* All rights reserved. Copyright (c) 
* Email: krlozadan@gmail.com
*
*/

using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameOverScreen : UIScreen 
{
	
	[SerializeField]
	private Text scoreText;
	
	private Animator anim;
	
	#region Screen Life Cycle

		protected override void UIScreenEnabled() 
		{
			int previousHighScore = PlayerPrefs.GetInt("Score", 0);
			int currentScore = GameManager.Instance.GetScore();
			if (currentScore > previousHighScore) 
			{
				scoreText.text = "New High Score -> " + currentScore + ", Congratulations!";
				PlayerPrefs.SetInt("Score", currentScore);
				PlayerPrefs.Save();
			} else 
			{
				scoreText.text = "Your score was: " + currentScore + ". The best is " + previousHighScore + ". Try Again!";
			}
			anim = GetComponentInChildren<Animator>();
			anim.SetInteger("Ending", Random.Range(1,3));
			AkSoundEngine.StopAll();
			AkSoundEngine.PostEvent("Play_Death", gameObject);
		}

		protected override void UIScreenDisabled()
		{
			GameManager.Instance.ResumeGame();
		}

	#endregion

	#region Events

		public void OnRestartGameButtonPressed()
		{
			SceneManager.LoadScene("Game");
			AudioManager.Instance.PlayEvent("Play_Audience_Loop_1");
			UIManager.Instance.ShowScreen<GameScreen>();
		}
		
		public void OnGoToMenuButtonPressed()
		{
			SceneManager.LoadScene("MainMenu");
			UIManager.Instance.ShowScreen<MainMenuScreen>();
		}

    #endregion
}
