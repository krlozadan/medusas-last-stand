﻿/*
* 
* Carlos Adan Cortes De la Fuente
* All rights reserved. Copyright (c) 
* Email: krlozadan@gmail.com
*
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameScreen : UIScreen 
{
    [Header("Kills & Score")] 
    [SerializeField]
    private Text killText;
    [SerializeField]
    private Text scoreText;

    [Header("Petrify Counter")]
    [SerializeField]
    private GameObject petrifyPowerIcon;
    [SerializeField]
    private GameObject petrifyContainer;

    [Space]
    [SerializeField]
    private GameObject healthBar;
    [SerializeField]
    private Color fullhealth;
    [SerializeField]
    private Color lowhealth;
    

    #region Screen Life Cycle

        protected override void UIScreenEnabled() 
        {
            Cursor.visible = false;
            AudioManager.Instance.PlayEvent("Stop_Music_Loop_Menu");
        }
        
        protected override void UIScreenDisabled()
        {
            Cursor.visible = true;
        }

    #endregion

    private void Update()
    {

        if((Input.GetKeyDown(KeyCode.Escape)))
		{
            UIManager.Instance.ShowScreen<PauseScreen>();
		}

        if(GameManager.Instance.IsGameOver())
        {
            StartCoroutine(ShowGameOverScreen());
        }
    }

    public void SetPowerIcons(int powersLeft)
    {
        foreach (Transform child in petrifyContainer.transform) 
        {
            GameObject.Destroy(child.gameObject);
        }

        for(int i = 0; i < powersLeft; i++)
        {
            GameObject newIcon = Instantiate(petrifyPowerIcon, Vector3.zero, Quaternion.identity);
            newIcon.transform.SetParent(petrifyContainer.transform);
            newIcon.transform.localScale = Vector3.one;
        }
    }

    public void SetKills(int count)
    {
        killText.text = "Kills: " + count;
    }
    
    public void SetScore(int count)
    {
        scoreText.text = "Score: " + count;
    }

    public void SetHealth(float health)
    {
        Vector3 newScale = healthBar.transform.localScale;
        newScale.x = health / 100f;
        healthBar.transform.localScale = newScale;
        healthBar.GetComponent<Image>().color = Color.Lerp(lowhealth, fullhealth, newScale.x);
    }

    IEnumerator ShowGameOverScreen()
    {
        yield return new WaitForSeconds(0.5f);
        UIManager.Instance.ShowScreen<GameOverScreen>();
        yield return null;
    }

}
