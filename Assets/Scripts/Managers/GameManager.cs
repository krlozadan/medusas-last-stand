﻿using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine;
using System.Collections.Generic;

public class GameManager : SingletonBehaviour<GameManager> 
{
	
	#region Global Variables
		
		private bool gameOver = false;
		private bool gamePaused = false;
		
		[SerializeField]
    	private int scoreMultiplier = 10;
		private int totalKills = 0;
		private int killsNoPetrified = 0;

		private int healthPBodyCount = 0;
		private int powerUpBodyCount = 0;
		
		[SerializeField]
		private int powerUpBonusBodyCount = 5;
		[SerializeField]
		private int healthBonusBodyCount = 10;
		
		private PetrifyPower power;
		private PickupSpawner pickupSpawner;
		private GameScreen gameScreen;
		private PlayerController player;

	#endregion


	#region Singleton Object

		protected override void SingletonAwake()
		{
			gameScreen = UIManager.Instance.GetScreen<GameScreen>();
			power = FindObjectOfType<PetrifyPower>();
			pickupSpawner = FindObjectOfType<PickupSpawner>();
			player = FindObjectOfType<PlayerController>();
		}

	#endregion 

	private void Update()
	{
		gameOver = !player.IsPlayerAlive();
		gameScreen.SetKills(killsNoPetrified);
		gameScreen.SetScore(GetScore());
	}

	public int GetScore()
	{
		return totalKills * scoreMultiplier;
	}

	#region Petrify Power Logic

		public void EnemyKilled(bool countsForPetrifyPower)
		{
			totalKills++;
			healthPBodyCount++;

			if(countsForPetrifyPower)
			{
				killsNoPetrified++;
				powerUpBodyCount++;
			}

			// Gives the player a power up
			if (powerUpBodyCount == powerUpBonusBodyCount)
			{
				powerUpBodyCount = 0;
				power.AddPower();
			}

			if(healthPBodyCount == healthBonusBodyCount)
			{
				healthPBodyCount = 0;
				pickupSpawner.SpawnPickup();
			}
		}

	#endregion

	#region Game Pause Logic

		public void PauseGame()
		{
			Time.timeScale = 0f;
			gamePaused = true;
		}
		
		public void ResumeGame()
		{
			Time.timeScale = 1f;
			gamePaused = false;
		}

		public bool IsGamePaused()
		{
			return gamePaused;
		}

	#endregion

	#region Game Over Logic

		public bool IsGameOver()
		{
			return gameOver;
		}

	#endregion

}
