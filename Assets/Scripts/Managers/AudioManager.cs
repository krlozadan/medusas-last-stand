﻿/*
* 
* Carlos Adan Cortes De la Fuente
* All rights reserved. Copyright (c) 
* Email: krlozadan@gmail.com
*
*/
using UnityEngine;
using System;
using UnityEngine.Audio;

public class AudioManager : SingletonBehaviour<AudioManager>
{


	protected override void SingletonAwake()
	{

	}

	#region Sound / Music Handling Methods

	
		public void PlayEvent(string name)
		{
			try {
				AkSoundEngine.PostEvent(name, gameObject);
			} catch (Exception e)
			{
				print("Exception: " + e);
			}
		}

		public void StopEvent(string name)
		{
			AkSoundEngine.PostEvent(name, gameObject);
		}

		public void StopAll()
		{
			AkSoundEngine.StopAll();
		}

    #endregion
}