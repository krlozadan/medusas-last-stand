using System.Collections;
using UnityEngine;
using UnityEngine.PostProcessing;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(SpriteRenderer))]
[RequireComponent(typeof(Collider2D))]
public class PlayerController : MonoBehaviour , IDamageable {

	//region keyword acts like struct with single initialization
	#region GlobalVars

        [Header("Camera Shake")]
		[SerializeField]
		private float duration;
		[SerializeField]
		private float magnitude;
		private CameraShake			_CameraShake;
		[Space]
		
		[SerializeField]
        private float           _MoveSpeed;

		[Tooltip("Put the width and height of the world here. Bit of a hack, but it'll do.")]
		[SerializeField]
		private float 			_XRange;
		[SerializeField]
		private float			_YRange;

		[Tooltip("After taking damage you are invulnerable for this long")]
		[SerializeField]
		private float 			_ImmunityTime = 5f;

		[SerializeField]
		private GameObject		_BloodParticles;
		
		[SerializeField]
		private Crosshairs		_Crosshair;

		private bool            _IsAlive = true;
		private Collider2D[]    _Cols;

		private bool			_IsFiring = false;

		// set each frame, only needed for the tail animations as of right now
		private bool			_IsMoving = false;				

        private Rigidbody2D         _RB;
		private Animator            _Anim;
		private WeaponHolder		_WH;
		private TailAnimationController		_TAnimCont;

        private float               _XVel;
		private float               _YVel;
		private Vector2				_MousePosition;

		private bool				_Invulnerable = false;

		private Health				_Health;

		private GameScreen			_GS;

		private PostProcessingProfile _PPP;
		
		

    #endregion GlobalVars

	private void Awake()
	{
        _Anim = GetComponent<Animator>();
		_RB = GetComponent<Rigidbody2D>();
		_WH = GetComponentInChildren<WeaponHolder>();
		_TAnimCont = GetComponentInChildren<TailAnimationController>();
		_Health = GetComponent<Health>();
		_GS = UIManager.Instance.GetScreen<GameScreen>();
		_PPP = Camera.main.GetComponent<PostProcessingBehaviour>().profile;
		_CameraShake = FindObjectOfType<CameraShake>();
	}

    void Update()
	{
		// Update the player living state
		_IsAlive =_Health.GetHealth() > 0;

		UpdateUI();

        // Really, we could potentially just hardcode these, instead of fiddling around with the editor settings
        StoreInput();
        
		Move();

		RotateToMouse();

		HandleFiring();

		SetAnimations();
		

        // Add Animation state code here
	}

	public bool IsPlayerAlive()
	{
		return _IsAlive;
	}

	void SetAnimations(){
		_Anim.SetBool("isHurt", _Invulnerable);
		_TAnimCont.SetBlinking(_Invulnerable);
		_Anim.SetBool("isFiring", _IsFiring);
		_TAnimCont.SetTailAnimationState(_IsMoving);
		_IsFiring = false;
	}

    private void StoreInput()
    {
        /// WASD movement
        _XVel = 0; _YVel = 0;

		if(!GameManager.Instance.IsGamePaused() && !GameManager.Instance.IsGameOver()){
			if(Input.GetKey(KeyCode.A))
			{
				_XVel -= _MoveSpeed;
			}
			if (Input.GetKey(KeyCode.D))
			{
				_XVel += _MoveSpeed;
			}

			if(Input.GetKey(KeyCode.W))
			{
				_YVel += _MoveSpeed;
			}
			if(Input.GetKey(KeyCode.S))
			{
				_YVel -= _MoveSpeed;
			}

			// more audio, get first frame of state change.
			if(_XVel!=0 || _YVel!=0){
				_IsMoving = true;
			}else{
				_IsMoving = false;
			}
		}
    }

	private void RotateToMouse(){
		if(!GameManager.Instance.IsGamePaused() && !GameManager.Instance.IsGameOver()){

			Camera c = Camera.main;
			Vector3 msPos = c.ScreenToWorldPoint(Input.mousePosition);
			_MousePosition = msPos;

			Vector2 distance = _MousePosition - (Vector2)transform.position;
			float angle = Mathf.Atan2(distance.y, distance.x) * Mathf.Rad2Deg;
			angle -= 90;
			transform.eulerAngles = new Vector3(0, 0, angle);
		}
	}

    private void Move()
	{
		Vector2 vel = new Vector2(_XVel, _YVel);
		_RB.velocity = vel;

		// If the position is out of bounds, move the player back
		// in bounds.
		Vector2 pos = transform.position;
		if(pos.x > _XRange) pos.x = _XRange; if(pos.x < -_XRange) pos.x = -_XRange;
		if(pos.y > _YRange) pos.y = _YRange; if(pos.y < -_YRange) pos.y = -_YRange;

        transform.position = pos;
	}

	// Some of this code is duplicated in Crosshairs.cs. Perhaps give us a reference to the crosshairs, and just aim towards that.
	// Also, obviously there's going to be some complicated logic here, Carlos is working on that. this is just for testing.
	private void HandleFiring(){

		bool lmb = Input.GetMouseButton(0);
		bool rmb = Input.GetMouseButton(1);

		_Crosshair.SetShooting((lmb | rmb) && !_WH.IsWeaponOverheated());

		// for animations
		_IsFiring = lmb | rmb;

		if (Input.GetMouseButton(0))
		{
			_IsFiring = true;
			_WH.Attack(0, transform.rotation);
		}

		// Now do shotgun.
		if (Input.GetMouseButton(1))
		{
			_IsFiring = true;
			_WH.Attack(1, transform.rotation);
		}
	}

    public void TakeDamage(float damage)
    {
		if(!_Invulnerable) {
			_Invulnerable = true;
			StartCoroutine(_CameraShake.Shake(duration, magnitude));
			_Health.TakeDamage(damage);
			// play sounds, animations, change state, etcetera.
			Instantiate(_BloodParticles, transform.position, transform.rotation);

			AkSoundEngine.PostEvent("Play_Efforts", gameObject);

			Invoke("RemoveInvulnerability", _ImmunityTime);
		}
    }

	private void RemoveInvulnerability()
	{
		_Invulnerable = false;
	}

	private void UpdateUI()
	{
		_Crosshair.SetOverheat(_WH.GetWeaponStatus());
		_GS.SetHealth(_Health.GetHealth());
		// Post Processing Effect
		ChromaticAberrationModel.Settings ca = _PPP.chromaticAberration.settings;
		ca.intensity = _WH.GetWeaponStatus();
		_PPP.chromaticAberration.settings = ca;
	}

}
