﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/****************************************************************
* Melee enemy makes a beeline for the player, so it needs a reference to 
* the player. When it gets close it attacks.
**************************************************************** */

public class MeleeEnemyController : Enemy {

	protected override void EnemyAwake(){}


	private void OnTriggerEnter2D(Collider2D other)
	{
		StonedEnemy stonedEnemy = other.gameObject.GetComponent<StonedEnemy>();
		if(stonedEnemy != null && !GameManager.Instance.IsGameOver())
		{
			Attack();
		}
	}

}