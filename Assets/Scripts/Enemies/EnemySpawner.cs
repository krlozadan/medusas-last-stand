﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour 
{
	[Header("Difficulty Level Control")]
	[SerializeField]
	private float decreaseOverTimeStep = 0.1f;
	[SerializeField]
	private float timeToIncreaseDifficulty = 15f;
	[SerializeField]	
	private float timeBetweenSpawn = 2f;

	[SerializeField]
	private Enemy[] enemyTypes;
	[SerializeField]
	private Transform[] spawnLocations;
	
	private float spawnDownLimit = 0.25f;

	internal static List<Enemy> enemies;

	private void Awake()
	{
		enemies = new List<Enemy>();
		StartCoroutine(SpawningEnemies());
		StartCoroutine(IncreaseDifficulty());
	}

	public void Reset()
	{
		enemies.Clear();
	}

	IEnumerator SpawningEnemies()
	{
		while(true)
		{
			yield return new WaitForSeconds(timeBetweenSpawn);
			int enemyIndex = Random.Range(0, enemyTypes.Length);
			int locationIndex = Random.Range(0, spawnLocations.Length);
			enemies.Add(Instantiate(enemyTypes[enemyIndex], spawnLocations[locationIndex].position, spawnLocations[locationIndex].rotation));
		}
	}

	IEnumerator IncreaseDifficulty()
	{
		while(true)
		{
			yield return new WaitForSeconds(timeToIncreaseDifficulty);
			if(timeBetweenSpawn > spawnDownLimit)
			{
				timeBetweenSpawn -= decreaseOverTimeStep;
			}
			else if(timeBetweenSpawn < spawnDownLimit)
			{
				timeBetweenSpawn = spawnDownLimit;
			}
			else 
			{
				yield break;
			}
			yield return null;
		}
	}
}
