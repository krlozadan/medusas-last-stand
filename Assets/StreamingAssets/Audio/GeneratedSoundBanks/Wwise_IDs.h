/////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Audiokinetic Wwise generated include file. Do not edit.
//
/////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef __WWISE_IDS_H__
#define __WWISE_IDS_H__

#include <AK/SoundEngine/Common/AkTypes.h>

namespace AK
{
    namespace EVENTS
    {
        static const AkUniqueID PLAY_AUDIENCE_LOOP_1 = 4065488625U;
        static const AkUniqueID PLAY_AUDIENCE_WIND = 2753598905U;
        static const AkUniqueID PLAY_BODY_IMPACTS = 1991322598U;
        static const AkUniqueID PLAY_CRUMBLES = 3766884109U;
        static const AkUniqueID PLAY_DEATH = 1172822028U;
        static const AkUniqueID PLAY_DEATH_SCREAMS = 622168011U;
        static const AkUniqueID PLAY_EFFORTS = 1712135591U;
        static const AkUniqueID PLAY_GAME_MX_LOOP = 2537396413U;
        static const AkUniqueID PLAY_IN_AIR_LOOPS = 2107350138U;
        static const AkUniqueID PLAY_MACHINEGUN = 2892089465U;
        static const AkUniqueID PLAY_MEDUSA_MOVMENT_LOOP = 2748654223U;
        static const AkUniqueID PLAY_MENU_MX_LOOP = 1875579082U;
        static const AkUniqueID PLAY_MUSIC_LOOP_MENU = 1901367914U;
        static const AkUniqueID PLAY_PUNS = 1565037570U;
        static const AkUniqueID PLAY_SHOTGUN = 992244U;
        static const AkUniqueID PLAY_SPEAR_IMPACTS = 3512450317U;
        static const AkUniqueID PLAY_SPELL = 826898240U;
        static const AkUniqueID PLAY_SWORD_IMPACTS = 3974852901U;
        static const AkUniqueID PLAY_SWORD_SWINGS = 2007279353U;
        static const AkUniqueID PLAY_THROW = 1977644338U;
        static const AkUniqueID PLAY_TURN = 3206947759U;
        static const AkUniqueID STOP_GAME_MX_LOOP = 4239786407U;
        static const AkUniqueID STOP_MACHINEGUN = 1004915535U;
        static const AkUniqueID STOP_MEDUSA_MOVMENT_LOOP = 389589073U;
        static const AkUniqueID STOP_MENU_MX_LOOP = 799092580U;
        static const AkUniqueID STOP_MUSIC_LOOP_MENU = 3240686296U;
    } // namespace EVENTS

    namespace GAME_PARAMETERS
    {
        static const AkUniqueID SS_AIR_FEAR = 1351367891U;
        static const AkUniqueID SS_AIR_FREEFALL = 3002758120U;
        static const AkUniqueID SS_AIR_FURY = 1029930033U;
        static const AkUniqueID SS_AIR_MONTH = 2648548617U;
        static const AkUniqueID SS_AIR_PRESENCE = 3847924954U;
        static const AkUniqueID SS_AIR_RPM = 822163944U;
        static const AkUniqueID SS_AIR_SIZE = 3074696722U;
        static const AkUniqueID SS_AIR_STORM = 3715662592U;
        static const AkUniqueID SS_AIR_TIMEOFDAY = 3203397129U;
        static const AkUniqueID SS_AIR_TURBULENCE = 4160247818U;
    } // namespace GAME_PARAMETERS

    namespace BANKS
    {
        static const AkUniqueID INIT = 1355168291U;
        static const AkUniqueID MADUSA = 2325799746U;
    } // namespace BANKS

    namespace BUSSES
    {
        static const AkUniqueID MASTER_AUDIO_BUS = 3803692087U;
    } // namespace BUSSES

    namespace AUDIO_DEVICES
    {
        static const AkUniqueID NO_OUTPUT = 2317455096U;
        static const AkUniqueID SYSTEM = 3859886410U;
    } // namespace AUDIO_DEVICES

}// namespace AK

#endif // __WWISE_IDS_H__
